﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankomatApplication
{
    public static class Repository
    {
        public static void FillDataSetRepository()
        {
            DataSet bankAccounts = new DataSet("BankAcounts");
            DataTable usersTable = new DataTable("Users");
            bankAccounts.Tables.Add(usersTable);

            DataColumn idColumn = new DataColumn("Id", Type.GetType("System.Int32"));
            idColumn.Unique = true;
            idColumn.AllowDBNull = false;
            idColumn.AutoIncrement = true;
            idColumn.AutoIncrementSeed = 1;
            idColumn.AutoIncrementStep = 1;

            DataColumn nameColumn = new DataColumn("Name", Type.GetType("System.String"));
            DataColumn surnameColumn = new DataColumn("Surname", Type.GetType("System.String"));
            DataColumn patronymicColumn = new DataColumn("Patronymic", Type.GetType("System.String"));
            DataColumn phoneColumn = new DataColumn("Phone", Type.GetType("System.String"));
            DataColumn passportDataColumn = new DataColumn("PassportData", Type.GetType("System.DateTime"));
            DataColumn loginColumn = new DataColumn("Login", Type.GetType("System.String"));
            loginColumn.AllowDBNull = false;
            DataColumn passwordColumn = new DataColumn("Password", Type.GetType("System.String"));
            passwordColumn.AllowDBNull = false;

            usersTable.Columns.Add(idColumn);
            usersTable.Columns.Add(nameColumn);
            usersTable.Columns.Add(surnameColumn);
            usersTable.Columns.Add(patronymicColumn);
            usersTable.Columns.Add(phoneColumn);
            usersTable.Columns.Add(passportDataColumn);
            usersTable.Columns.Add(loginColumn);
            usersTable.Columns.Add(passwordColumn);
            usersTable.PrimaryKey = new DataColumn[] { usersTable.Columns["Id"] };

            usersTable.Rows.Add(new object[] { null, "Александр", "Бородач", "Родионович", "89153265849", "03.03.2019", "login1", "pass" });


        }

    }
}
