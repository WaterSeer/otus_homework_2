﻿using System;

namespace BankomatApplication
{
    enum OperationType
    {
        AddingBallance,
        SubbingBalance
    }
    class History
    {
        public int Id { get; set; }
        public DateTime OperationDate { get; set; }
        public OperationType OperationType { get; set; }
        public decimal Sum { get; set; }
        public Account AccountId { get; set; }
    }
}
