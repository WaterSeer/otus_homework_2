﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankomatApplication
{
    /// <summary>
    /// Счета. хранит информацию о счетах польователей, у каждого пользователя может быть несколько счетов.
    /// </summary>
    class Account
    {
        public int Id { get; set; }
        public User UserId { get; set; }
        public DateTime OpenAccountData { get; set; }
        public decimal Balance { get; set; }
        public int MyProperty { get; set; }
        
    }
}
