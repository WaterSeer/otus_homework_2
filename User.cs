﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankomatApplication
{
    /// <summary>
    /// Класс Пользователь. Хранит информацию о пользователях, зарегестрированных в банке
    /// </summary>
    class User
    {
        public int Id { get; set; }
        public string  Name { get; set; }
        public string  Surname { get; set; }
        public string  Patronymic { get; set; }
        public string Phone { get; set; }
        public string  PassportData { get; set; }
        public DateTime RegisterDate { get; set; }
        public string  Login { get; set; }
        public string  Password { get; set; }
    }
}
